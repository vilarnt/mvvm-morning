//
//  ContentView.swift
//  MVVMLearningMorning
//
//  Created by Vilar Fiuza da Camara Neto on 12/12/22.
//

import SwiftUI

struct MovieListView: View {
    @ObservedObject private var viewModel: MovieListViewModel

    init(viewModel: MovieListViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        switch self.viewModel.movies {
        case .loading:
            Text("Loading…")
        case .failed:
            Text("Server error communication, sifu")
                .foregroundColor(.red)
        case .loaded(let movies):
            List(movies) { movie in
                VStack(alignment: .leading, spacing: 8) {
                    Text(movie.origTitle)
                        .bold()
                    if let localizedTitle = movie.localizedTitle {
                        Text(localizedTitle)
                    }
                    Text("\(movie.year)")
                        .foregroundColor(.gray)
                    if let duration = movie.duration {
                        Text("\(duration) minutes")
                            .foregroundColor(.gray)
                    }
                }
            }
        }
    }
}

class MovieListViewModel: ObservableObject {
    private let service: MovieServiceProtocol
    @Published var movies: AsyncData<[Movie]> = .loading

    init(service: MovieServiceProtocol) {
        self.service = service
        self.service.listMovies { movies in
            DispatchQueue.main.async {
                if let movies {
                    self.movies = .loaded(movies)
                } else {
                    self.movies = .failed
                }
            }
        }
    }
}

struct MovieListView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            let service = MockupMovieService()
            let viewModel = MovieListViewModel(service: service)
            MovieListView(viewModel: viewModel)
                .previewDisplayName("Normal")

            let loadingService = MockupMovieService(behaviour: .keepLoading)
            let loadingViewModel = MovieListViewModel(service: loadingService)
            MovieListView(viewModel: loadingViewModel)
                .previewDisplayName("Loading")

            let failedService = MockupMovieService(behaviour: .alwaysFails)
            let failedViewModel = MovieListViewModel(service: failedService)
            MovieListView(viewModel: failedViewModel)
                .previewDisplayName("Failure")
        }
    }
}

//
//  JSONMovieService.swift
//  MVVMLearningMorning
//
//  Created by Vilar Fiuza da Camara Neto on 13/12/22.
//

import Foundation


class JSONMovieService: MovieServiceProtocol {
    func listMovies(completionHandler: @escaping ([Movie]?) -> Void) {
        let url = URL(string: "http://localhost:8000/index.json")!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Houve um erro durante o download")
                completionHandler(nil)
                return
            }
            guard let data else {
                print("Não há dados")
                completionHandler(nil)
                return
            }
            
            do {
                let movies = try JSONDecoder().decode([Movie].self, from: data)
                completionHandler(movies)
            } catch {
                print("Não foi possível decodificar os dados")
                completionHandler(nil)
                return
            }
        }
        task.resume()
    }
}

//
//  MovieServiceProtocol.swift
//  MVVMLearningMorning
//
//  Created by Vilar Fiuza da Camara Neto on 13/12/22.
//

import Foundation

protocol MovieServiceProtocol {
    func listMovies(completionHandler: @escaping ([Movie]?) -> Void) -> Void
}

//
//  Movie.swift
//  MVVMLearningMorning
//
//  Created by Vilar Fiuza da Camara Neto on 12/12/22.
//

import Foundation


/**
 General information about a movie.
 */
struct Movie: Identifiable, Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case origTitle
        case localizedTitle = "altTitle"
        case year
        case duration
    }
    
    init(id: String, origTitle: String, localizedTitle: String? = nil, year: Int, duration: Int? = nil) {
        self.id = id
        self.origTitle = origTitle
        self.localizedTitle = localizedTitle
        self.year = year
        self.duration = duration
    }
    
    /// Unique identifier.
    let id: String
    /// Title in original language.
    let origTitle: String
    /// Localized title, if available.
    let localizedTitle: String?
    /// Release year.
    let year: Int
    /// Duration of the movie, in minutes.
    let duration: Int?
}

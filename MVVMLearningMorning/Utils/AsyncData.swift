//
//  AsyncData.swift
//  MVVMLearningMorning
//
//  Created by Vilar Fiuza da Camara Neto on 14/12/22.
//

import Foundation


enum AsyncData<Dados> {
    case loading
    case loaded(Dados)
    case failed
}

//
//  MVVMLearningMorningApp.swift
//  MVVMLearningMorning
//
//  Created by Vilar Fiuza da Camara Neto on 12/12/22.
//

import SwiftUI

@main
struct MVVMLearningMorningApp: App {
    var body: some Scene {
        WindowGroup {
            let service = JSONMovieService()
            let viewModel = MovieListViewModel(service: service)
            MovieListView(viewModel: viewModel)
        }
    }
}
